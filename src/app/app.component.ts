import { Component } from '@angular/core';
import * as moment from 'moment';
import * as moment_tz from 'moment-timezone';
import { Timezone } from '../app/../constants/timezone';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'customer-app';
  myDate: Date;
  public country;

  constructor(public timezone:Timezone) {
    // console.log(moment_tz().tz("America/Los_Angeles").format());
    // console.log(moment_tz().tz("Europe/Dublin").format());
    // let currentTime = moment_tz().tz("Europe/Dublin");
    console.log(this.timezone.currentTime.valueOf());
    // console.log(currentTime.valueOf());
    // console.log(moment_tz.tz.names());
    // this.country = moment_tz.tz.names();
    // let now = moment();
    // console.log('hello world', now.format());
    // console.log(now.add(7, 'days').format());
  }
}
