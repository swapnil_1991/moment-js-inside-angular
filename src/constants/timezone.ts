import { Injectable } from '@angular/core';
import * as moment_tz from 'moment-timezone';

Injectable()
export class Timezone  {
    currentTime = moment_tz().tz("Europe/Dublin");
}